﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text CountText;
    public Text WinText;
    private int count;
    private Rigidbody rb;
    public GameObject Pickup2;
    


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        WinText.text = "";
        
        
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
        if (other.gameObject.CompareTag("Floor"))
        {

            RestartLevel(); //restarts level if player falls and hits the floor
        }
    }    
    void SetCountText ()
    {
        CountText.text = "Count: " + count.ToString();
        if (count == 6)
        {
            print("spawn");
            Pickup2.SetActive(true);  //spawns pickups on second platform after player has finished collecting them on first platform
        }
        if (count >= 12)
        {
            WinText.text = "You Win!";

            Invoke("RestartLevel", 2f); //restarts level after all pickups are collected
        }
    }
    void RestartLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //restarts scene
    }
}
